#!/usr/bin/env python3

__author__ = "Mateusz Garbiak"

#subtraction
def subtraction():
    return 5-2

print(subtraction())

#sum
def sum(a, b, c):
    return a+b+c

print(sum(1,2,3))

#my name
def printMyName():
    print("My name is Mateusz")

printMyName()

#hello world
def printHelloWorld():
    print("Hello World")

printHelloWorld()

#class 1
class Class1:
    pass

def createClass1():
    return Class1()

newCreateClass1 = createClass1()
print(newCreateClass1)

#class fruit
class Fruit:
    def __init__(self):
        print("Create Fruit")

    def __del__(self):
        print("Destroy Fruit")

    def createFruit(self):
        fruit1=Fruit()
        return fruit1

class Apple(Fruit):
    pass

apple1 = Apple()
apple1.createFruit()